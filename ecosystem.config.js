const path = require('path');

module.exports = {
  apps: [
    {
      name: 'twitter-search',
      script: path.join(process.cwd(), 'dist/main.js'),
      instances  : 1,
      env: {
        NODE_ENV: 'production'
      },
    },
  ],
};
