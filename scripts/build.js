const { exec } = require('child-process-promise');
const fs = require('fs-extra');
const path = require('path');
const Ora = require('ora');

const pkg = require('../package.json');

(async () => {
  const loader = new Ora('build nest app')
  try {
    loader.start()
    const zipFilePath = path.join(process.cwd(), `${pkg.name}-v${pkg.version}.zip`)
    await fs.remove(path.join(process.cwd(), 'dist'));
    await fs.remove(zipFilePath);

    await exec('tsc -p tsconfig.prod.json');
    loader.text = 'build react app';
    await exec('cd src/@client && npm run build');
    loader.succeed('build succesfull');
  } catch (err) {
    loader.fail('build fail');
    console.error(err);
    process.exit(1);
  }
})();
