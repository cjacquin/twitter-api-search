const dotenv = require('dotenv');
const { promisify } = require('util');
const fs = require('fs');
const fsXtra = require('fs-extra');
const path = require('path');
const { exec } = require('child-process-promise');
const { argv } = require('yargs');
const Ora = require('ora');
const archiver = require('archiver');
const crypto = require('crypto');
const streamToPromise = require('stream-to-promise');
const axios = require('axios');

dotenv.config({
  path: path.join(__dirname, 'release.env'),
});

const uploadFileToNextcloud = (name, content) => {
  return axios({
    url: `https://${process.env.NEXTCLOUD_HOST}/remote.php/dav/files/${
      process.env.NEXTCLOUD_USER
    }/apps/twitter-search/${name}`,
    method: 'put',
    data: content,
    headers: {
      Authorization: `Basic ${Buffer.from(
        `${process.env.NEXTCLOUD_USER}:${process.env.NEXTCLOUD_PASS}`,
      ).toString('base64')}`,
    },
  });
};

(async () => {
  const loader = Ora('Install dependencies');

  try {
    loader.start();
    await exec('npm i');

    loader.text = 'build typescript';
    await exec('npm run build');

    loader.text = 'release, version, changelog ...';
    if (argv.preRelease)
      await exec(`npx standard-version --prerelease ${argv.preRelease}`);
    else if (argv.releaseAs)
      await exec(`npx standard-version --release-as ${argv.releaseAs}`);
    else await exec('npx standard-version');

    await exec('git push --follow-tags origin master');
    loader.text = 'compress ...';
    const { name, version } = require('../package.json');
    const tarFilePath = path.join(process.cwd(), `${name}-${version}.tar.gz`);
    const files = ['package.json', 'package-lock.json', 'ecosystem.config.js'];
    const dirs = ['dist', 'public', 'logs'];

    const archive = archiver('tar', {
      gzip: true,
    });

    await promisify(fs.mkdir)(path.join(process.cwd(), 'public', 'download'));

    for (const dir of dirs) {
      archive.directory(path.join(process.cwd(), dir), dir);
    }

    for (const file of files) {
      archive.file(path.join(process.cwd(), file), { name: file });
    }

    const ormConfig = await fsXtra.readJSON(
      path.join(process.cwd(), 'ormconfig.json'),
    );
    ormConfig.entities = ['dist/**/**.entity.js'];
    archive.append(JSON.stringify(ormConfig), { name: 'ormconfig.json' });

    const hash = crypto.createHash('sha256');
    hash.setEncoding('hex');

    archive.pipe(hash);
    archive.finalize();

    let chunks = [];
    archive.on('data', chunk => chunks.push(chunk.toString()));

    await streamToPromise(archive);
    const tarAsString = chunks.join('');
    hash.end();
    // read all file and pipe it (write it) to the hash object
    const tarUrl = `https://${
      process.env.NEXTCLOUD_HOST
    }/remote.php/dav/files/${
      process.env.NEXTCLOUD_USER
    }/apps/twitter-search/${path.basename(tarFilePath)}`;
    const appSrcContent = `SOURCE_URL=${tarUrl}
SOURCE_SUM=${hash.read()}
SOURCE_SUM_PRG=sha256sum
ARCH_FORMAT=tar.gz
SOURCE_IN_SUBDIR=true
SOURCE_FILENAME=${path.basename(tarFilePath)}`;

    await Promise.all([
      uploadFileToNextcloud('app.src', appSrcContent),
      uploadFileToNextcloud(path.basename(tarFilePath), tarAsString),
    ]);

    loader.succeed(`${name} v${version} sucessfuly released => ${tarUrl}`);
  } catch (err) {
    loader.fail('something fail :(');
    console.error(err);
  }
})();
