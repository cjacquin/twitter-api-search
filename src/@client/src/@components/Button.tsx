import glamorous from 'glamorous';
import React, { CSSProperties } from 'react';
import Loader from 'react-loader-spinner';

interface ButtonProps {
  type?: string;
  isLoading?: boolean;
  disabled?: boolean;
  onClick?: any;
  style?: CSSProperties;
}

const HtmlButton = glamorous.button<ButtonProps>(({ isLoading }) => ({
  display: 'flex',
  overflow: 'hidden',

  height: '50px',
  minWidth: '200px',

  margin: '5px',
  marginTop: '40px',
  padding: '5px 5px',

  cursor: 'pointer',
  textAlign: 'center',
  textDecoration: 'none !important',
  textTransform: 'none',
  transition: 'opacity 150ms linear',
  userZelect: 'none',
  whiteSpace: 'nowrap',
  // textTransform: 'capitalize',

  background: 'blue',
  border: '0 none',
  borderRadius: '4px',
  color: 'white',

  fontSize: 15,
  lineHeight: 1.3,

  MozAppearance: 'none',
  WebkitAppearance: 'none',
  appearance: 'none',

  alignItems: 'center',
  flex: '0 0 160px',
  justifyContent: 'center',

  boxShadow: `2px 5px 10px black`,

  opacity: isLoading ? 0.85 : 1,

  ':disabled': {
    cursor: 'not-allowed',
    opacity: 0.4,
  },

  ':hover:not([disabled])': {
    opacity: 0.85,
  },

  ':active:not([disabled])': {
    opacity: 0.75,
  },
}));

export const Button: React.SFC<ButtonProps> = ({
  children,
  isLoading,
  ...props
}) => (
  <HtmlButton {...props} isLoading={isLoading}>
    {isLoading ? (
      <Loader type="Oval" color="white" height={30} width={30} />
    ) : (
      children
    )}
  </HtmlButton>
);
