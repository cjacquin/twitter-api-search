import glamorous from 'glamorous';

export const Form = glamorous.form({
  width: '100%',
});

export const FieldWrapper = glamorous.div({
  marginBottom: '20px',
});
