import glamorous from 'glamorous';

export const H1 = glamorous.h1({});

export const H2 = glamorous.h2({});

export const Message = glamorous.p({});