import glamorous from 'glamorous';
import React, { CSSProperties } from 'react';

interface TextInputProps {
  id?: string;
  name?: string;
  width?: number;
  placeholder: string;
  type?: string;
  value?: string;
  valid?: boolean;
  touched?: boolean;
  onChange?: (evt: any) => any;
  onKeyUp?: (evt: any) => any;
  style?: CSSProperties;
  inputStyle?: CSSProperties;
}

export const TextInput: React.SFC<TextInputProps> = ({
  width,
  valid,
  touched,
  style,
  inputStyle,
  ...inputProps
}) => (
  <InputWrapper style={{ ...style, width }}>
    <Input {...inputProps} style={inputStyle} error={!valid && touched} />
  </InputWrapper>
);

const Input = glamorous.input<{ error?: boolean }>(({ error }) => ({
  backgroundColor: 'white',
  borderColor: error ? 'red' : 'grey',
  borderStyle: 'solid',
  borderWidth: 2,
  color: 'black',
  fontSize: 15,
  height: 40,
  paddingLeft: '15px',
  width: 'calc(100% - 15px)',
}));

const InputWrapper = glamorous.div({
  position: 'relative',
});
