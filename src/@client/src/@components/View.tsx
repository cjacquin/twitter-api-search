import glamorous from 'glamorous';

export const Container = glamorous.div({
  '@media(min-width: 600px)': {
    margin: 'auto auto',
    width: 500,
  },
  width: '100%',
});
