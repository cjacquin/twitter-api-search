const baseUrl = process.env.NODE_ENV === 'production' ? '/twitter-search/' : 'http://localhost:3000/'

export const httpService = {
  async requestFile(body: any) {
    try {
      const response = await fetch(`${baseUrl}api/v1/request`, {
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'POST',
      });  
      return response.json();
    } catch (err) {
      throw new Error('Server Error');
    }
  }
}
