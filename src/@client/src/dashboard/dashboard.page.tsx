import React from "react";
import { componentFromStream } from "recompose";
import { map } from "rxjs/operators";

import { H1, H2, Message } from "../@components/Headings";
import { Container } from "../@components/View";
import { store } from "../store";
import { TweetForm } from "./tweet.form";

const handleSubmit = (formData: any) =>
  store.dispatch({ requestFile: formData });

export const DashboardPage = componentFromStream(() =>
  store.pick("pending", "requestMessage").pipe(
    map(({ pending, requestMessage }) => (
      <Container>
        <H1>Twitter api search (Alpha)</H1>
        <H2>Tweet Data request</H2>
        <TweetForm onSubmit={handleSubmit} pending={pending} />
        {!!requestMessage && <Message>{requestMessage}</Message>}
      </Container>
    ))
  )
);
