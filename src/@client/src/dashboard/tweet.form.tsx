import React from "react";
import { rxForm } from "rx-react-form";

import { Button } from "../@components/Button";
import { FieldWrapper, Form } from "../@components/Form";
import { TextInput } from "../@components/TextInput";

/* tslint:disable */
const validateEmail = (email: string) => {
  const emailRegexp = new RegExp(
    /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
  );
  return emailRegexp.test(email) ? undefined : "invalid email";
};
/* tslint:enable */

@(rxForm as any)({
  fields: {
    email: {
      validation: validateEmail,
    },
    tweetId: {
      validation: (value: string) => {
        return (!value || value === '') && 'required'
      }
    },
  }
})
export class TweetForm extends React.PureComponent<any> {
  public render() {
    return (
      <Form noValidate={true}>
        <FieldWrapper>
          <TextInput name="tweetId" placeholder="tweet id" />
        </FieldWrapper>
        <FieldWrapper>
          <TextInput name="email" placeholder="your email" />
        </FieldWrapper>
        <Button
          type="submit"
          isLoading={this.props.pending}
          disabled={this.props.pending}
        >
          Request tweet data
        </Button>
      </Form>
    );
  }
}
