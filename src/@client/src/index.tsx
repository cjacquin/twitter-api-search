import { css } from 'glamor';
import React from 'react';
import ReactDOM from 'react-dom';
import { setObservableConfig } from 'recompose';
import { from, Observable } from 'rxjs';

import { App } from './App';
import registerServiceWorker from './registerServiceWorker';

css.global('html', { height: '100%', width: '100%' });

css.global('body', {
  '*': {
    outline: 'none',
  },
  height: '100%',
  margin: 0,
  width: '100%',
});

setObservableConfig({
  fromESObservable: from as any,
  toESObservable: (stream: Observable<any>) => stream,
});

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
