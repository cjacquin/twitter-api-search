import { createStore, silentLoggerOptions } from "lenrix";
import { mapTo, switchMap } from "rxjs/operators";

import { httpService } from "./@services/http";

const initialState = {
  pending: false,
  requestMessage: ""
};

const storeOptions = {
  logger: silentLoggerOptions
};

export const store = createStore(initialState, storeOptions)
  .actionTypes<{
    requestFile: any;
    requestFileSuccess: any;
  }>()
  .updates(_ => ({
    requestFile: () => _.focusPath("pending").setValue(true),
    requestFileSuccess: () =>
      _.setFields({
        pending: false,
        requestMessage:
          "Votre requete a bien ete prise en compte, vous recevrez un email avec un lien de telechargement sous peu"
      })
  }))
  .epics({
    requestFile: obs$ =>
      obs$.pipe(
        switchMap(httpService.requestFile),
        mapTo({ requestFileSuccess: "" })
      )
  });
