import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { TwitterModule } from './twitter/twitter.module';
import { CommonModule } from './common/common.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    TwitterModule,
    CommonModule,
  ],
})
export class AppModule {}
