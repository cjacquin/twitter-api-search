export enum Env {
  PROD = 'production',
  DEV = 'development',
  TEST = 'test',
}
