import { Module } from '@nestjs/common';
import { MailerModule } from '@nest-modules/mailer';

import { configProvider } from './config.provider';
import { LoggerMiddleware } from './logger.middleware';
import { MailerConfigService } from './mailerConfig.service';
import { Logger } from './logger.service';

@Module({
  exports: [Logger, LoggerMiddleware, configProvider],
  imports: [
    MailerModule.forRootAsync({
      useClass: MailerConfigService,
      imports: [CommonModule],
    }),
  ],
  providers: [Logger, LoggerMiddleware, configProvider],
})
export class CommonModule {}
