import path from 'path';

import { ConfigService } from './config.service';

export const configProvider = {
  provide: ConfigService,
  useValue: new ConfigService(path.join(process.cwd(), '.env')),
};
