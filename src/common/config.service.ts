import path from 'path';
import Joi from 'joi';
import fs from 'fs';
import dotenv from 'dotenv';

import { emailRegexp, hostRegexp, urlRegex } from './helpers';
import { Env } from './Env';

export interface EnvConfig {
  [key: string]: string;
}

export class ConfigService {
  private readonly envConfig: EnvConfig;

  rateLimit = {
    max: 100,
    timeWindow: '1 minute',
  };
  staticServer = {
    root: path.join(process.cwd(), 'public'),
  };

  constructor(filePath: string) {
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = this.validateInput(config);
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'staging'])
        .default('development'),
      PORT: Joi.number().default(3000),
      HOST: Joi.string()
        .regex(hostRegexp)
        .default('0.0.0.0'),
      APP_URL: Joi.string()
        .regex(urlRegex)
        .required(),

      TWITTER_TOKEN: Joi.string().required(),
      TWITTER_TOKEN_SECRET: Joi.string().required(),
      TWITTER_CONSUMER_KEY: Joi.string().required(),
      TWITTER_CONSUMER_SECRET: Joi.string().required(),
      TWITTER_SEARCH_LIMIT: Joi.number().default(10000),

      MAILER_HOST: Joi.string()
        .regex(hostRegexp)
        .required(),
      MAILER_PORT: Joi.number().required(),
      MAILER_SECURE: Joi.boolean(),
      MAILER_USERNAME: Joi.string().required(),
      MAILER_PASS: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  get env() {
    return this.envConfig.NODE_ENV;
  }

  get port() {
    return Number(this.envConfig.PORT);
  }

  get appUrl() {
    return this.envConfig.APP_URL;
  }

  get host() {
    return this.envConfig.HOST;
  }

  get twitterConfig() {
    return {
      consumer_key: this.envConfig.TWITTER_CONSUMER_KEY,
      consumer_secret: this.envConfig.TWITTER_CONSUMER_SECRET,
      access_token: this.envConfig.TWITTER_TOKEN,
      access_token_secret: this.envConfig.TWITTER_TOKEN_SECRET,
      strictSSL: true,
    };
  }

  get twitterSearchLimit() {
    return Number(this.envConfig.TWITTER_SEARCH_LIMIT);
  }

  get mailerConfig() {
    return {
      transport: {
        host: this.envConfig.MAILER_HOST,
        secureConnection: !!this.envConfig.MAILER_SECURE,
        port: this.envConfig.MAILER_PORT,
        auth: {
          user: this.envConfig.MAILER_USERNAME,
          pass: this.envConfig.MAILER_PASS,
        },
      },
      defaults: {
        from: `"twitter-api search" <${this.envConfig.MAILER_USERNAME}>`,
      },
      tls: {
        rejectUnauthorized: false,
      },
    };
  }

  isProduction() {
    return this.envConfig.NODE_ENV === Env.PROD;
  }

  isDevelopment() {
    return this.envConfig.NODE_ENV === Env.DEV;
  }

  isTest() {
    return this.envConfig.NODE_ENV === Env.TEST;
  }
}
