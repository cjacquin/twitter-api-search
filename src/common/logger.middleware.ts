import {
  Injectable,
  NestMiddleware,
  MiddlewareFunction,
  Logger,
} from '@nestjs/common';
import onHeaders from 'on-headers';
import { promisify } from 'util';
import chalk from 'chalk';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  constructor(private readonly logger: Logger) {}

  resolve(): MiddlewareFunction {
    return async (req, res, next) => {
      const startAt = Date.now();
      next();
      await promisify(onHeaders)(res);

      const diff = chalk.italic.underline.yellow(`+${Date.now() - startAt}ms`);
      const statusCode =
        res.statusCode < 400
          ? chalk.green(res.statusCode)
          : chalk.red(res.statusCode);
      const method = chalk.bold.cyan(req.method);
      const path = chalk.bold.blue(req.originalUrl);

      this.logger.log(
        `${method} ${path} ${statusCode} ${diff}`,
      );
    };
  }
}
