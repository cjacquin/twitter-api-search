import { Injectable, LoggerService } from '@nestjs/common';
import path from 'path';
import winston, { Logger as WinstonLogger } from 'winston';

import { ConfigService } from './config.service';

@Injectable()
export class Logger implements LoggerService {
  logger: WinstonLogger;

  constructor(private readonly config: ConfigService) {
    this.logger = winston.createLogger({
      level: 'info',
      format: winston.format.json(),
      transports: [
        new winston.transports.File({
          filename: path.join(process.cwd(), 'logs/error.log'),
          level: 'error',
          maxsize: 5000000,
        }),
        new winston.transports.File({
          filename: path.join(process.cwd(), 'logs/combined.log'),
          maxsize: 5000000,
        }),
      ],
    });

    if (this.config.isDevelopment()) {
      this.logger.add(
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.timestamp(),
            winston.format.align(),
            winston.format.printf(info => {
              const { timestamp, level, message, ...args } = info;

              const ts = timestamp.slice(0, 19).replace('T', ' ');
              return `${ts} [${level}]: ${message} ${
                Object.keys(args).length ? JSON.stringify(args, null, 2) : ''
              }`;
            }),
          ),
        }),
      );
    }
  }

  log(message): void {
    this.logger.info(message);
  }

  warn(message: string): void {
    this.logger.warn(message);
  }

  error(error: string): void {
    this.logger.error(error);
  }
}
