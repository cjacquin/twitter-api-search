import { Injectable } from '@nestjs/common';
import {
  MailerOptionsFactory,
  MailerModuleOptions,
} from '@nest-modules/mailer';

import { ConfigService } from './config.service';

@Injectable()
export class MailerConfigService implements MailerOptionsFactory {
  constructor(private readonly config: ConfigService) {}

  createMailerOptions(): MailerModuleOptions {
    return this.config.mailerConfig;
  }
}
