import {
  SwaggerModule,
  DocumentBuilder,
  SwaggerDocument,
} from '@nestjs/swagger';
import { INestApplication, INestFastifyApplication } from '@nestjs/common';
import { readJSON } from 'fs-extra';
import path from 'path';

export const initSwagger = async (
  app: INestApplication & INestFastifyApplication,
): Promise<SwaggerDocument> => {
  const { version } = await readJSON(path.join(process.cwd(), 'package.json'));

  const options = new DocumentBuilder()
    .setTitle('Twitter api search')
    .setDescription('Return a zip file containing 2 csv files with retweets and replies to a given tweet')
    .setVersion(version)
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/v1/docs', app, document);

  return document;
};
