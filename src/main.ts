import { NestFactory, FastifyAdapter } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import compress from 'fastify-compress';
import rateLimit from 'fastify-rate-limit';
import helmet from 'fastify-helmet';

import { AppModule } from './app.module';
import { initSwagger } from './common/swagger';
import { ConfigService } from './common/config.service';
import { Logger } from './common/logger.service';

(async () => {
  let logger: Logger;
  try {
    const app = await NestFactory.create(AppModule, new FastifyAdapter(), {
      logger: false,
    });
    logger = app.get(Logger);
    const config = app.get(ConfigService);

    app.useLogger(logger);
    app.useGlobalPipes(new ValidationPipe());
    app.useStaticAssets(config.staticServer);
    if (config.isDevelopment()) app.enableCors();

    app.register(rateLimit, config.rateLimit);
    app.register(helmet);
    app.register(compress, { global: true });

    await initSwagger(app);

    await app.listen(config.port, config.host);

    logger.log(
      `App is accessible here ${config.appUrl}/api/v1 in ${config.env} environment`,
    );
  } catch (err) {
    logger.error(err.message);
    process.exit(1);
  }
})();
