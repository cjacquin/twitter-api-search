import fs from 'fs';
import path from 'path';
import { Injectable } from '@nestjs/common';
import { MailerProvider } from '@nest-modules/mailer';
import { interval, BehaviorSubject } from 'rxjs';
import { filter, merge, delay, retryWhen, take } from 'rxjs/operators';
import streamToPromise from 'stream-to-promise';
import { CronJob } from 'cron';
import autobind from 'autobind-decorator';
import { SendMailOptions } from 'nodemailer';

import { TwitterService, Tweet } from './twitter.service';
import { CsvService } from './csv.service';
import { ConfigService } from '../common/config.service';
import { Logger } from '../common/logger.service';
import { FileRequestService } from './fileRequest.service';
import { FileRequest, FileRequestState } from './fileRequest.entity';

@Injectable()
export class CronService {
  private isWorking = false;
  private job: CronJob;

  constructor(
    private readonly logger: Logger,
    private readonly twitterService: TwitterService,
    private readonly csvService: CsvService,
    private readonly fileRequestService: FileRequestService,
    private readonly config: ConfigService,
    private readonly mailer: MailerProvider,
  ) {
    new BehaviorSubject(null)
      .pipe(
        delay(3000),
        merge(interval(30 * 60 * 1000)),
        filter(() => !this.isWorking),
        retryWhen(errors =>
          errors.pipe(
            delay(5 * 60 * 1000),
            take(3),
          ),
        ),
      )
      .subscribe(this.processFileRequests);

    this.job = new CronJob('0 0 */30 * *', this.cleanDb);
  }

  private async cleanDb() {
    if (!this.job.running) {
      const fileRequests = await this.fileRequestService.getOutdatedRequests();
      Promise.all(fileRequests.map(_ => this.fileRequestService.remove(_)));
    }
  }

  private async fromTwitterToZip(fileRequest: FileRequest): Promise<string> {
    const retweets = await this.twitterService.getRetweets(fileRequest.tweetId);
    let replies: Tweet[];

    try {
      replies = await this.twitterService.getReplies(fileRequest.tweetId);
    } catch (err) {
      this.logger.error(err);
      replies = [];
    }

    this.logger.log('archive ...');
    const fileName = `${fileRequest.tweetId}-${Date.now()}.zip`;
    const zipStream = this.csvService.createArchive(replies, retweets);
    this.logger.log('stream created');
    const writeStream = fs.createWriteStream(
      path.join(process.cwd(), 'public/download', fileName),
    );
    const stream = zipStream.pipe(writeStream);
    zipStream.finalize();
    await streamToPromise(stream);
    this.logger.log('archive created');
    return fileName;
  }

  @autobind
  private async processFileRequests(): Promise<void> {
    this.isWorking = true;
    const queuedJobs = await this.fileRequestService.getQueuedRequest();

    if (queuedJobs.length > 0)
      this.logger.log(`processing ${queuedJobs.length} file requests`);

    for (const job of queuedJobs) {
      const emailConfig: SendMailOptions = {
        to: job.email,
        from: this.config.mailerConfig.transport.auth.user,
        subject: `Mise a disposition des données pour le tweet ${
          job.tweetId
        }`,
      };
      try {
        this.logger.log(`start generating files for tweet ${job.tweetId}`);
        job.state = FileRequestState.PENDING;

        await this.fileRequestService.createOrUpdateRequest(job);

        const fileName = await this.fromTwitterToZip(job);

        job.file = `${this.config.appUrl}/download/${fileName}`;
        job.state = FileRequestState.COMPLETED;

        emailConfig.html =  `<p>Les fichiers csv pour le tweet ${
          job.tweetId
        } sont telechargeable <a href="${job.file}">ici</a></p>`;

        await this.mailer.sendMail(emailConfig);
        await this.fileRequestService.createOrUpdateRequest(job);

        this.logger.log('twitter job ended succesfully');
      } catch (err) {
        this.logger.error(err.message);
        job.errorCount++;
        job.state = FileRequestState.ERROR;
        if (job.errorCount > 3) {
          await this.fileRequestService.remove(job);
          emailConfig.html = `<p>Erreur, Les fichiers csv pour le tweet ${
            job.tweetId
          } n'ont pas pu être créer</p>`;
          emailConfig.subject = `Erreur avec le tweet ${
            job.tweetId
          }`;
          await this.mailer.sendMail(emailConfig);
        } else {
          await this.fileRequestService.createOrUpdateRequest(job);
        }
      }
    }
    this.isWorking = false;
  }
}
