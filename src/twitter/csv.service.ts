import { Injectable } from '@nestjs/common';
import { parse as json2csv } from 'json2csv';
import { Readable } from 'stream';
import archiver, { Archiver } from 'archiver';

import { Tweet } from './twitter.service';

@Injectable()
export class CsvService {
  private archive: Archiver;

  private appendFile(content: string, name: string) {
    const stream = new Readable({
      read() {
        this.push(content);
        this.push(null);
      },
    });

    this.archive.append(stream, { name });
  }

  createArchive(replies: Tweet[], retweets: Tweet[]): Archiver {
    this.archive = archiver('zip', {
      zlib: { level: 9 },
    });

    if (replies.length > 0)
      this.appendFile(
        json2csv(replies, { fields: Object.keys(replies[0]) }),
        'replies.csv',
      );

    if (retweets.length > 0)
      this.appendFile(
        json2csv(retweets, { fields: Object.keys(retweets[0]) }),
        'retweets.csv',
      );

    return this.archive;
  }
}
