import { Entity, Column, PrimaryGeneratedColumn, UpdateDateColumn, CreateDateColumn } from 'typeorm';

export enum FileRequestState {
  QUEUE = 'queue',
  PENDING = 'pending',
  COMPLETED = 'completed',
  ERROR = 'error',
}

@Entity()
export class FileRequest {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ length: 50 })
  email: string;

  @Column({ length: 30 })
  tweetId: string;

  @Column({ length: 10 })
  state: FileRequestState;

  @Column({ length: 50 })
  file?: string;

  @Column({ default: 0 })
  errorCount: number;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updateAt?: Date;
}