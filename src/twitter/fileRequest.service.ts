import fs from 'fs';
import { promisify } from 'util';
import path from 'path';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Raw } from 'typeorm';
import { flatten } from 'ramda';
import { subDays } from 'date-fns';

import { FileRequest, FileRequestState } from './fileRequest.entity';

@Injectable()
export class FileRequestService {
  constructor(
    @InjectRepository(FileRequest)
    private readonly requestRepository: Repository<FileRequest>,
  ) {}

  getOutdatedRequests(): Promise<FileRequest[]> {
    return this.requestRepository.find({
      where: { state: FileRequestState.COMPLETED, updateAt: Raw(_ => `${_} < ${subDays(new Date(), 7).toDateString()}`) },
    });
  }

  async getQueuedRequest(): Promise<FileRequest[]> {
    const fileRequestMatrix = await Promise.all([
      this.requestRepository.find({
        where: { state: FileRequestState.ERROR },
      }),
      this.requestRepository.find({
        where: { state: FileRequestState.PENDING },
      }),
      this.requestRepository.find({
        where: { state: FileRequestState.QUEUE },
      }),
    ]);

    return flatten<FileRequest>(fileRequestMatrix);
  }

  createOrUpdateRequest(request: FileRequest): Promise<FileRequest> {
    return this.requestRepository.save(request);
  }

  async remove(fileRequest: FileRequest) {
    await promisify(fs.unlink)(
      path.join(
        process.cwd(),
        'public',
        'download',
        path.basename(fileRequest.file),
      ),
    );
    return this.requestRepository.remove(fileRequest);
  }
}
