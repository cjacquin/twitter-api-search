import {
  Controller,
  Post,
  Body,
  UseInterceptors,
  ClassSerializerInterceptor,
} from '@nestjs/common';

import { FileRequestService } from './fileRequest.service';
import { FileRequestState, FileRequest } from './fileRequest.entity';
import { FileRequestDto } from './twitter.dto';

@Controller('api/v1/request')
export class TwitterController {
  constructor(private readonly requestService: FileRequestService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Post()
  async requestCsv(
    @Body() { tweetId, email }: FileRequestDto,
  ): Promise<FileRequest> {
    return this.requestService.createOrUpdateRequest({
      tweetId,
      state: FileRequestState.QUEUE,
      email,
      file: '',
      errorCount: 0,
    });
  }
}
