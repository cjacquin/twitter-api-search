import { IsNotEmpty, IsString, IsEmail } from 'class-validator';

export class FileRequestDto {
  @IsNotEmpty()
  @IsString()
  tweetId: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;
}