import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FileRequestService } from './fileRequest.service';
import { TwitterService } from './twitter.service';
import { CsvService } from './csv.service';
import { TwitterController } from './twitter.controller';
import {  FileRequest } from './fileRequest.entity';
import { CronService } from './cron.service';
import { LoggerMiddleware } from '../common/logger.middleware';
import { CommonModule } from '../common/common.module';

@Module({
  exports: [FileRequestService],
  imports: [TypeOrmModule.forFeature([FileRequest]), CommonModule],
  controllers: [TwitterController],
  providers: [FileRequestService, TwitterService, CsvService, CronService],
})
export class TwitterModule implements NestModule {
  public configure(consumer: MiddlewareConsumer): void {
    consumer.apply(LoggerMiddleware).forRoutes(TwitterController);
  }
}
