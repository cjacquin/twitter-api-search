import { promisify } from 'util';
import querystring from 'querystring';
import { Injectable } from '@nestjs/common';
import Twit from 'twit';
import { Status } from 'twitter-d';
import { addDays, format } from 'date-fns';
import Ora from 'ora';

import { flattenObject } from '../common/helpers';
import { ConfigService } from '../common/config.service';
import { Logger } from '../common/logger.service';

interface TwitterStatus extends Status {
  text: string;
  retweeted_status: TwitterStatus;
}

interface TwitResponseArray {
  data: {
    statuses: TwitterStatus[];
  };
  resp: any;
}

interface TwitResponse {
  data: TwitterStatus;
  resp: any;
}

export interface Tweet extends TwitterStatus {}

@Injectable()
export class TwitterService {
  twitterClient: Twit;
  constructor(
    private readonly logger: Logger,
    private readonly config: ConfigService,
  ) {
    // super(config.twitterConfig);
    this.twitterClient = new Twit(config.twitterConfig);
  }

  private getMaxId(totalSearch: Tweet[]) {
    return !!totalSearch[totalSearch.length - 1]
      ? totalSearch[totalSearch.length - 1].id
      : null;
  }

  private formatTweet(tweet: Status): Tweet {
    return flattenObject(tweet) as Tweet;
  }

  private shouldNotFetchMoreSearch(
    totalSearch: Tweet[],
    tweetDate: Date,
    statuses: Status[],
  ) {
    return (
      statuses.length === 0 ||
      tweetDate.getTime() >
        (!!totalSearch[totalSearch.length - 1]
          ? new Date(totalSearch[totalSearch.length - 1].created_at).getTime()
          : tweetDate.getTime()) ||
      totalSearch.length > this.config.twitterSearchLimit
    );
  }

  private async searchForTweets(
    options: any,
    tweetDate: Date,
    loader: any,
  ): Promise<Tweet[]> {
    const totalSearch = [];
    let fetchMore = true;
    while (fetchMore) {
      const url = `search/tweets.json?${querystring.stringify({
        ...options,
        count: 100,
        max_id: this.getMaxId(totalSearch),
        until: format(addDays(tweetDate, 3), 'YYYY-MM-DD'),
      })}`;

      try {
        const search = await this.twitterClient.get(url) as TwitResponseArray;

        search.data.statuses
          .filter(_ => !!_)
          .forEach(data => totalSearch.push(data));

        fetchMore = !this.shouldNotFetchMoreSearch(
          totalSearch,
          tweetDate,
          search.data.statuses,
        );
      } catch (err) {
        this.logger.warn(err.message);
        await promisify(setTimeout)(15 * 60000);
      } finally {
        loader.text = `tweets fetched => ${totalSearch.length}`;
      }
    }
    return totalSearch;
  }

  async getRetweets(tweetId: string): Promise<Tweet[]> {
    const loader = new Ora('fetch replies ...');
    loader.start();

    const tweet = await this.twitterClient.get(
      `statuses/show/${tweetId}`,
    ) as  TwitResponse;
    const username = tweet.data.user.screen_name;
    const tweetDate = new Date(tweet.data.created_at);
    const text = tweet.data.text;

    const totalSearch = await this.searchForTweets(
      {
        q: `RT @${username}`,
      },
      tweetDate,
      loader,
    );

    loader.succeed(`retweets fetched successfully => ${totalSearch.length}`);

    return totalSearch
      .filter(
        ({ retweeted_status }) =>
          !!retweeted_status && retweeted_status.text === text,
      )
      .map(this.formatTweet);
  }

  async getReplies(tweetId: string): Promise<Tweet[]> {
    const loader = new Ora('fetch replies ...');
    loader.start();
    const tweet = await this.twitterClient.get(
      `statuses/show/${tweetId}`,
    ) as TwitResponse;
    const username = tweet.data.user.screen_name;
    const tweetDate = new Date(tweet.data.created_at);

    const totalSearch = await this.searchForTweets(
      {
        q: `@${username}`,
        until: format(addDays(tweetDate, 3), 'YYYY-MM-DD'),
      },
      tweetDate,
      loader,
    );

    loader.succeed(`replies fetched successfully => ${totalSearch.length}`);

    return totalSearch
      .filter(
        ({ in_reply_to_status_id }) =>
          in_reply_to_status_id === Number(tweetId),
      )
      .map(this.formatTweet);
  }
}
